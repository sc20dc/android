package com.example.mytrafficlights;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {


    private LinearLayout light1;
    private LinearLayout light2;
    private LinearLayout light3;
    private Button but;
    private boolean ststop;
    private String butOn;
    private String butOff;
    private  int counter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counter = 0;
        ststop = false;
        light1 = findViewById(R.id.light1);
        light2 = findViewById(R.id.light2);
        light3 = findViewById(R.id.light3);
        but = findViewById(R.id.buttonS);
        butOn = getString(R.string.startstop1);
        butOff = getString(R.string.startstop2);

        light1.setBackground(getResources().getDrawable(R.color.Gray));
        light2.setBackground(getResources().getDrawable(R.color.Gray));
        light3.setBackground(getResources().getDrawable(R.color.Gray));


        but.setText(butOn);
        but.setTextColor(getResources().getColor(R.color.Green));

    }





    public void onClickStart(View view){


        if(ststop == false){

            but.setText(butOff);
            but.setTextColor(getResources().getColor(R.color.Red));
            ststop = true;
            counter = 0;

           new Thread(new Runnable() {
                @Override
                public void run() {

                    while(ststop == true){




                        counter++;
                        switch (counter){
                            case 1:
                                light1.setBackground(getResources().getDrawable(R.color.Red));
                                light2.setBackground(getResources().getDrawable(R.color.Gray));
                                light3.setBackground(getResources().getDrawable(R.color.Gray));
                                break;
                            case 2:
                                light1.setBackground(getResources().getDrawable(R.color.Gray));
                                light2.setBackground(getResources().getDrawable(R.color.Yellow));
                                light3.setBackground(getResources().getDrawable(R.color.Gray));
                                break;
                            case 3:
                                light1.setBackground(getResources().getDrawable(R.color.Gray));
                                light2.setBackground(getResources().getDrawable(R.color.Gray));
                                light3.setBackground(getResources().getDrawable(R.color.Green));
                                break;

                        }

                        if(counter == 3){counter = 0;}

                        try{
                            Thread.sleep(1000);
                        }catch (InterruptedException e) {
                            e.printStackTrace();
                        }


                    }
                }
            }).start();


        } else{
            ststop = false;
            but.setText(butOn);
            but.setTextColor(getResources().getColor(R.color.Green));

            light1.setBackground(getResources().getDrawable(R.color.Gray));
            light2.setBackground(getResources().getDrawable(R.color.Gray));
            light3.setBackground(getResources().getDrawable(R.color.Gray));
            counter = -1;
          Thread.currentThread().interrupt();

        }


    }

    public void onDestroy(){
        super.onDestroy();
        ststop = false;

    }


}